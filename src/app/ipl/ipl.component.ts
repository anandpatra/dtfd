import { Component, OnInit, ViewChild } from '@angular/core';
import { Player } from '../services/player';
import { PlayerServiceService } from '../services/player-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import {MatTable} from '@angular/material/table';

@Component({
  selector: 'app-ipl',
  template: `./ipl.component.html`,
  styleUrls: ['./ipl.component.css']
})
export class IplComponent implements OnInit {

  playerDetaisl: Player[] = [];
  input = new FormControl('');
  displayedColumns: string[] = ['player_id', 'player_name', 'dob', 'batting_hand', 'bowling_skill', 'country'];

  dataSource: MatTableDataSource<Player>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Player>;


  constructor(private playerService: PlayerServiceService) {
    this.dataSource = new MatTableDataSource(this.playerDetaisl);
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getPlayers();
  }

  ngAfterViewInit() {
  }

  getPlayers() {
    this.playerService.getPlayersFromJson().subscribe(data => {
      this.playerDetaisl = data;
      this.dataSource = new MatTableDataSource(this.playerDetaisl);
    });
    console.log(this.dataSource.data);
  }

  applyFilter(event: Event) {
    console.log(event)
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
