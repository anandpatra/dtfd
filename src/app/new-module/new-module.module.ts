import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { NewModuleComponent } from './new-module.component';
import { AppRoutingModule } from '../app-routing.module';
import { NewModuleRoutingModule } from './new-module-routing.module';



@NgModule({
  declarations: [
    ProfileComponent,
    NewModuleComponent
  ],
  imports: [
    NewModuleRoutingModule,
    CommonModule
  ]
})
export class NewModuleModule { }
