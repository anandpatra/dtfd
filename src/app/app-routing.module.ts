import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { IplComponent } from './ipl/ipl.component';
import { NewComponentComponent } from './new-component/new-component.component';

const routes: Routes = [
  {path: '', component: IplComponent},
  {path: 'ipl', component: IplComponent},
  {path: 'forms', component: NewComponentComponent},
  {path: 'module', loadChildren: () => import('././new-module/new-module.module').then(m => m.NewModuleModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
