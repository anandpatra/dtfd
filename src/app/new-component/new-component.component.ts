import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../services/user';

@Component({
  selector: 'app-new-component',
  templateUrl: './new-component.component.html',
  styleUrls: ['./new-component.component.css']
})
export class NewComponentComponent implements OnInit {

  userReg: User[] = [];

  profileForm: FormGroup;
  address: FormGroup


  company = new FormControl('heraizen', Validators.required);

  constructor(private formBuilder: FormBuilder) {
    this.userReg = JSON.parse(localStorage.getItem("userData"));
    if (this.userReg === null) {
      this.userReg = []
    }
  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', [Validators.email, Validators.pattern("")]],
      address: this.formBuilder.group({
        street: [''],
        city: [''],
        state: ['']
      })
    });
  }


  submit() {
    this.userReg.push(this.profileForm.value);
    console.log(this.profileForm['value']);
    localStorage.setItem("userData", JSON.stringify(this.userReg));
  }

  

}
