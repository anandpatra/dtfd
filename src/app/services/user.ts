export interface User {
    firstName: string;
    lastName: string;
    address: Address
}

class Address {
    state: string;
    city: string;
    address: string;
}