import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from './player';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerServiceService {

  constructor( private http : HttpClient) { }

  getPlayersFromJson(): Observable<Player[]>{
     return this.http.get<any[]>('https://lwl-ems.herokuapp.com/api/ems/all')
    //  .pipe(
    //   catchError(this.handleError<Hero[]>('getHeroes', []))
    //  );
  }

 

}
