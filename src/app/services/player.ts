export interface Player {
    player_id: number,
    player_name: string,
    dob: Date,
    batting_hand: string,
    bowling_skill: string,
    country: string,
    is_umpire: string
}